# OpenML dataset: credit-score-classification-Hzl

https://www.openml.org/d/45938

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

person credit-related information

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45938) of an [OpenML dataset](https://www.openml.org/d/45938). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45938/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45938/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45938/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

